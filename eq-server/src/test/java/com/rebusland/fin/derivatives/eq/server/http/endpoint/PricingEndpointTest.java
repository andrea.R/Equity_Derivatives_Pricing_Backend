package com.rebusland.fin.derivatives.eq.server.http.endpoint;

import java.io.IOException;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.ClassPathResource;

import com.rebusland.fin.derivatives.eq.server.http.request.model.PricingRequest;
import com.rebusland.fin.derivatives.eq.server.http.response.model.PricingResponse;
import com.rebusland.fin.derivatives.eq.server.input.model.Derivative;
import com.rebusland.fin.derivatives.eq.server.input.model.FlatMarketData;
import com.rebusland.fin.derivatives.eq.server.input.model.MonteCarloSettings;
import com.rebusland.fin.derivatives.eq.server.output.model.FairPricingOutput;
import com.rebusland.fin.derivatives.eq.server.output.model.PricingResults;
import com.rebusland.fin.derivatives.eq.server.output.model.StatisticalResults;

/**
 * TODO parameterize!!!
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PricingEndpointTest {
	private static final String baseUrl = "http://localhost";

	// private static final String resourcesLocation = "src/test/resources";
	private static final String pricingRelativeLocation = "pricing";

	// TODO This could be parametric (Junit parametric tests)
	private static final String test01 = "asianOption";
	private static final String test02 = "europeanVanillaOption";

	// Pricing inputs
	private static final String inputFolder = "input";
	private static final String marketData = "flatMarketData.json";
	private static final String product = "product.json";
	private static final String monteCarloSettings = "monteCarloSettings.json";

	private static final String testPath;
	static {
		testPath = String.join("/", /*resourcesLocation,*/ pricingRelativeLocation, test01, inputFolder);
	}

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	private PricingRequest mapInputsToPricingRequest() throws IOException {
		// return new ClassPathResource(testPath).getFile();
		final Derivative prod = Derivative.fromJsonFile(new ClassPathResource(testPath + "/" + product).getFile(), Derivative.class);
		final FlatMarketData mktData = FlatMarketData.fromJsonFile(new ClassPathResource(testPath + "/" + marketData).getFile(), FlatMarketData.class);
		final MonteCarloSettings mcSettings = MonteCarloSettings.fromJsonFile(new ClassPathResource(testPath + "/" + monteCarloSettings).getFile(), MonteCarloSettings.class);

		return PricingRequest.builder()
			.id(1L)
			.product(prod)
			.mktData(mktData)
			.monteCarloSettings(mcSettings)
			.build();
	}

	@Test
	public void readInputs() throws IOException {
		final String request = mapInputsToPricingRequest().toJson();
		System.out.println(request);
	}

	@Test
	public void pricingEndpointIntegrationTest() throws Exception {
		final String pricingEndpointUrl = String.join("", baseUrl, ":", String.valueOf(port), "/", UrlPaths.PRICING);

		final Derivative prod = Derivative.fromJsonFile(new ClassPathResource(testPath + "/" + product).getFile(), Derivative.class);
		final FlatMarketData mktData = FlatMarketData.fromJsonFile(new ClassPathResource(testPath + "/" + marketData).getFile(), FlatMarketData.class);
		final MonteCarloSettings mcSettings = MonteCarloSettings.fromJsonFile(new ClassPathResource(testPath + "/" + monteCarloSettings).getFile(), MonteCarloSettings.class);

		final PricingRequest pricingReq = PricingRequest.builder()
			.id(1L)
			.product(prod)
			.mktData(mktData)
			.monteCarloSettings(mcSettings)
			.build();

		final PricingResponse pricingResp = this.restTemplate.postForObject(pricingEndpointUrl, pricingReq, PricingResponse.class);
// 		TODO add assertion on expected results
//		final PricingResponse expectedPricingResp = PricingResponse.builder()
//			.id(1L)
//			.pricingResults(PricingResults.builder()
//				.price(FairPricingOutput.builder().fair(0.1).monteCarloError(0.001).moments(0.1, 0.2).build())
//				.delta(FairPricingOutput.builder().fair(0.02).monteCarloError(0.0001).moments(0.03, 0.04).build())
//				.build())
//			.build();
//
//		Assertions.assertThat(pricingResp).isEqualTo(expectedPricingResp);
	}
}
