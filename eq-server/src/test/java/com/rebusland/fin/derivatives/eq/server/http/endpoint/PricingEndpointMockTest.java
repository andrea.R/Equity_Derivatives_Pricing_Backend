package com.rebusland.fin.derivatives.eq.server.http.endpoint;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.rebusland.fin.derivatives.eq.server.enums.model.CallPut;
import com.rebusland.fin.derivatives.eq.server.enums.model.SimulationScheduler;
import com.rebusland.fin.derivatives.eq.server.enums.model.VarianceReduction;
import com.rebusland.fin.derivatives.eq.server.enums.model.VariateGenerator;
import com.rebusland.fin.derivatives.eq.server.http.request.model.PricingRequest;
import com.rebusland.fin.derivatives.eq.server.http.response.model.PricingResponse;
import com.rebusland.fin.derivatives.eq.server.input.model.Derivative;
import com.rebusland.fin.derivatives.eq.server.input.model.FlatMarketData;
import com.rebusland.fin.derivatives.eq.server.input.model.GreeksSettings;
import com.rebusland.fin.derivatives.eq.server.input.model.MonteCarloSettings;
import com.rebusland.fin.derivatives.eq.server.input.model.Underlying;
import com.rebusland.fin.derivatives.eq.server.input.model.VanillaOption;
import com.rebusland.fin.derivatives.eq.server.output.model.FairPricingOutput;
import com.rebusland.fin.derivatives.eq.server.output.model.PricingResults;
import com.rebusland.fin.derivatives.eq.server.output.model.StatisticalResults;

@WebMvcTest(PricingEndpoint.class)
public class PricingEndpointMockTest {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testPricingEndpointWebLayer() throws Exception {
		final PricingRequest req = PricingRequest.builder()
			.id(1L)
			.product(Derivative.builder()
				.underlying(Underlying.builder().alias("ALIAS").spotPrice(12.3).refPrice(12.3).build())
				.issueDate(0L)
				.expiryDate(1L)
				.payoffId("EUROPEAN_STD")
				.vanillaOption(VanillaOption.builder().callPut(CallPut.CALL).strike(10.0).build())
				.build())
			.mktData(FlatMarketData.builder()
				.rfRate(0.1)
				.volatility(0.2)
				.build())
			.monteCarloSettings(MonteCarloSettings.builder()
				.greeks(GreeksSettings.builder().spotRelativeShift(0.01).build())
				.seed(1L)
				.simulationScheduler(SimulationScheduler.MULTITHREAD)
				.varianceReduction(VarianceReduction.ANTITHETIC)
				.variateGenerator(VariateGenerator.STD_GAUSS)
				.nSimulations(10000)
				.nThreads(1)
				.build())
			.build();

//		final PricingResponse expectedPricingResp = PricingResponse.builder()
//			.id(1L)
//			.pricingResults(PricingResults.builder()
//				.price(FairPricingOutput.builder().fair(0.1).monteCarloError(0.001).moments(0.1, 0.2).build())
//				.delta(FairPricingOutput.builder().fair(0.02).monteCarloError(0.0001).moments(0.03, 0.04).build())
//				.build())
//			.build();

		this.mockMvc.perform(MockMvcRequestBuilders
			.post("/" + UrlPaths.PRICING)
			.contentType(MediaType.APPLICATION_JSON)
			.content(req.toJson()))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.notNullValue()))
		.andExpect(MockMvcResultMatchers.jsonPath("$.pricingResults", Matchers.notNullValue()))
		.andExpect(MockMvcResultMatchers.jsonPath("$.pricingResults.price", Matchers.notNullValue()))
		.andExpect(MockMvcResultMatchers.jsonPath("$.pricingResults.delta", Matchers.notNullValue()));
		// .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString(expectedPricingResp.toJson())));
	}
}
