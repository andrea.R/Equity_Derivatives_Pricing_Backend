package com.rebusland.fin.derivatives.eq.server.pricing.jni;

import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import com.rebusland.fin.derivatives.eq.server.enums.model.AverageType;
import com.rebusland.fin.derivatives.eq.server.enums.model.CallPut;
import com.rebusland.fin.derivatives.eq.server.enums.model.SimulationScheduler;
import com.rebusland.fin.derivatives.eq.server.enums.model.VarianceReduction;
import com.rebusland.fin.derivatives.eq.server.enums.model.VariateGenerator;
import com.rebusland.fin.derivatives.eq.server.http.request.model.PricingRequest;
import com.rebusland.fin.derivatives.eq.server.input.model.AsianOption;
import com.rebusland.fin.derivatives.eq.server.input.model.Derivative;
import com.rebusland.fin.derivatives.eq.server.input.model.FlatMarketData;
import com.rebusland.fin.derivatives.eq.server.input.model.GreeksSettings;
import com.rebusland.fin.derivatives.eq.server.input.model.MonteCarloSettings;
import com.rebusland.fin.derivatives.eq.server.input.model.Underlying;
import com.rebusland.fin.derivatives.eq.server.input.model.VanillaOption;
import com.rebusland.fin.derivatives.eq.server.output.model.PricingResults;

public class PricingJNILayerTest {

	@Test
	public void testJNICallToPricingLib() throws InterruptedException, UnsupportedEncodingException {
		final PricingRequest req = PricingRequest.builder()
			.id(1L)
			.product(Derivative.builder()
				.underlying(Underlying.builder().alias("EVILCORPINC").spotPrice(8.0).refPrice(8.0).build())
				.issueDate(0L)
				.expiryDate(1000L)
//				.payoffId("ASIAN")
//				.asianOption(AsianOption.builder()
//					.callPut(CallPut.CALL)
//					.strikeFixingDates(2L, 3L)
//					.avgTypeStrike(AverageType.ARITHMETIC)
//					.priceFixingDates(995L, 996L)
//					.avgTypePrice(AverageType.ARITHMETIC)
//					.build())
				.payoffId("EUROPEAN_STD")
				.vanillaOption(VanillaOption.builder().callPut(CallPut.CALL).strike(10.0).build())
				.build())
			.mktData(FlatMarketData.builder()
				.rfRate(0.000119047619047619)
				.volatility(0.00881917103688197)
				.build())
			.monteCarloSettings(MonteCarloSettings.builder()
				.greeks(GreeksSettings.builder().spotRelativeShift(0.0001).build())
				.seed(1L)
				.simulationScheduler(SimulationScheduler.MULTITHREAD)
				.varianceReduction(VarianceReduction.ANTITHETIC)
				.variateGenerator(VariateGenerator.STD_GAUSS)
				.nSimulations(10000)
				.nThreads(4)
				.build())
			.build();

		System.out.println(req.toJson(false));
		final String result = PricingJNILayerHandler.EvaluateBlack(req.getMktData().toJson(), req.getMonteCarloSettings().toJson(), req.getProduct().toJson());
		System.out.print(PricingResults.fromJson(result, PricingResults.class).toJson(false));
	}

	/**
	 * The rules managing the shared library memory mapping when loaded by the application should be the following:
	 *  - The shared libraries text (code) segment is shared among different processes, but this should not
	 *    pose problems since the text segment refers to the shared lib instructions and not to its state.
	 *  - As the shared lib data segment (consisting of static and global variables, i.e. the state of the lib)
	 *    a different copy is made for each PROCESS, i.e. the virtual address space of each process receives
	 *    its own independent copy of the data segment: there is no inter-process sharing of data segments
	 *    if not explicitly specified.
	 *  - On the other hand, multiple THREADS spawned by each process share the dynamic library data segment!
	 *    Therefore, for each process, global resources defined within the shared lib loaded by the process should
	 *    be synchronized/managed in case of concurrent calls.
	 *  - Each THREAD has its own independent stack memory space for shared library method executions.    
	 */
	@Test
	public void testConcurrentCallPricingLib() throws InterruptedException, UnsupportedEncodingException {
		final MonteCarloSettings mcSettings = MonteCarloSettings.builder()
			.greeks(GreeksSettings.builder().spotRelativeShift(0.0001).build())
			.seed(1L)
			.simulationScheduler(SimulationScheduler.MULTITHREAD)
			.varianceReduction(VarianceReduction.ANTITHETIC)
			.variateGenerator(VariateGenerator.STD_GAUSS)
			.nSimulations(10000000)
			.nThreads(4)
			.build();
		final FlatMarketData mktData = FlatMarketData.builder()
			.rfRate(0.000119047619047619)
			.volatility(0.00881917103688197)
			.build();

		final PricingRequest req1 = PricingRequest.builder()
			.id(1L)
			.product(Derivative.builder()
				.underlying(Underlying.builder().alias("EVILCORPINC").spotPrice(8.0).refPrice(8.0).build())
				.issueDate(0L)
				.expiryDate(1000L)
				.payoffId("ASIAN")
				.asianOption(AsianOption.builder()
					.callPut(CallPut.CALL)
					.strikeFixingDates(2L, 3L)
					.avgTypeStrike(AverageType.ARITHMETIC)
					.priceFixingDates(995L, 996L)
					.avgTypePrice(AverageType.ARITHMETIC)
					.build())
				.build())
			.mktData(mktData)
			.monteCarloSettings(mcSettings)
			.build();

		final PricingRequest req2 = PricingRequest.builder()
			.id(1L)
			.product(Derivative.builder()
				.underlying(Underlying.builder().alias("EVILCORPINC").spotPrice(8.0).refPrice(8.0).build())
				.issueDate(0L)
				.expiryDate(1000L)
				.payoffId("EUROPEAN_STD")
				.vanillaOption(VanillaOption.builder().callPut(CallPut.CALL).strike(10.0).build())
				.build())
			.mktData(mktData)
			.monteCarloSettings(mcSettings)
			.build();

		final PricingRequest req3 = PricingRequest.builder()
				.id(1L)
				.product(Derivative.builder()
					.underlying(Underlying.builder().alias("DARTHVADERCORP").spotPrice(31.0).refPrice(31.0).build())
					.issueDate(0L)
					.expiryDate(1500L)
					.payoffId("EUROPEAN_STD")
					.vanillaOption(VanillaOption.builder().callPut(CallPut.PUT).strike(27.0).build())
					.build())
				.mktData(mktData)
				.monteCarloSettings(mcSettings)
				.build();	

		final PricingRequest req4 = PricingRequest.builder()
				.id(1L)
				.product(Derivative.builder()
					.underlying(Underlying.builder().alias("GREEDYSHARKS").spotPrice(2.0).refPrice(2.0).build())
					.issueDate(0L)
					.expiryDate(1000L)
					.payoffId("EUROPEAN_STD")
					.vanillaOption(VanillaOption.builder().callPut(CallPut.CALL).strike(1.5).build())
					.build())
				.mktData(mktData)
				.monteCarloSettings(mcSettings)
				.build();

		// SEND CONCURRENTLY MULTIPLE REQUESTS TO THE PRICING SHARED LIB
		// We expect the global state of the shared library to be shared by the two threads
		// (and messed up by concurrent calls if proper synchronization constraints are not implemented within the dynamic library)
		List<String> results = new ArrayList<>();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				final String out = PricingJNILayerHandler.EvaluateBlack(req1.getMktData().toJson(), req1.getMonteCarloSettings().toJson(), req1.getProduct().toJson());
				results.add(out);
				System.out.println("Thread 1 finished");
			}
		});

		Thread t2 = new Thread(new Runnable() {
			@Override
			public void run() {
				final String out = PricingJNILayerHandler.EvaluateBlack(req2.getMktData().toJson(), req2.getMonteCarloSettings().toJson(), req2.getProduct().toJson());
				results.add(out);
				System.out.println("Thread 2 finished");
			}
		});

		Thread t3 = new Thread(new Runnable() {
			@Override
			public void run() {
				final String out = PricingJNILayerHandler.EvaluateBlack(req3.getMktData().toJson(), req3.getMonteCarloSettings().toJson(), req3.getProduct().toJson());
				results.add(out);
				System.out.println("Thread 3 finished");
			}
		});

		Thread t4 = new Thread(new Runnable() {
			@Override
			public void run() {
				final String out = PricingJNILayerHandler.EvaluateBlack(req4.getMktData().toJson(), req4.getMonteCarloSettings().toJson(), req4.getProduct().toJson());
				results.add(out);
				System.out.println("Thread 4 finished");
			}
		});

		t1.run();
		t2.run();
		t3.run();
		t4.run();
		results.forEach(out -> System.out.println(out));
	}
}
