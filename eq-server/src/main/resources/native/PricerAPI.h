#ifndef _PRICER_API_H_
#define _PRICER_API_H_

#include "SHARED_API.h"

namespace pricer_api {

	/**
	 * C compliant APIs decoupling the outside-world calls to the shared library
	 * from the actual endpoints implementation.
	 * The destination buffer, where result is stored, must be managed and injected by the caller,
	 * in order to enforce the reentrancy and thread safety of the library endpoints,
	 * which could be called concurrently within the same process of the caller.
	 * Thus, it's expected that the injected buffer is not a shared resource, but local to each thread.
	 * It's also assumed that the buffer's capacity can fit the whole result's content.
	 * The pointer to the output buffer is also returned for convenience.
	 */
	extern "C" {

		char* SHARED_API EvaluateBlack(
			const char* flatMktDataJSONString,
			const char* mcSettingsJSONString,
			const char* productJSONString,
			char* destinationBuffer, // where to put the results
			const unsigned int destinationBufferCapacity // the capacity of the output buffer
		);

	}

}

#endif
