#include "com_rebusland_fin_derivatives_eq_server_pricing_jni_PricingJNILayerHandler.h"

// the native library
#include "../native/PricerAPI.h"

static constexpr unsigned int BUFFER_SIZE = 1024 * 8; // 8KB buffer  

/**
 * This JNI layer creates a buffer where the shared library could store the result. The buffer is local to the method stack,
 * and should therefore guarantee that retrievals of the output from the shared library are both reentrant and thread safe.
 */
JNIEXPORT jstring JNICALL Java_com_rebusland_fin_derivatives_eq_server_pricing_jni_PricingJNILayerHandler_EvaluateBlack(JNIEnv *env, jclass obj, jstring flatMktDataJSONString, jstring mcSettingsJSONString, jstring productJSONString) {
    const char* flatMktDataJSONCharPtr = env->GetStringUTFChars(flatMktDataJSONString, NULL);
 	const char* mcSettingsJSONCharPtr = env->GetStringUTFChars(mcSettingsJSONString, NULL);
    const char* productJSONCharPtr = env->GetStringUTFChars(productJSONString, NULL);

	char localBuffer[BUFFER_SIZE];
	pricer_api::EvaluateBlack(flatMktDataJSONCharPtr, mcSettingsJSONCharPtr, productJSONCharPtr, localBuffer, BUFFER_SIZE);
	return env -> NewStringUTF(localBuffer);
}
