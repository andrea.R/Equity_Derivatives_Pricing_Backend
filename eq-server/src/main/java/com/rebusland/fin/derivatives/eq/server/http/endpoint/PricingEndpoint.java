package com.rebusland.fin.derivatives.eq.server.http.endpoint;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rebusland.fin.derivatives.eq.server.http.request.model.PricingRequest;
import com.rebusland.fin.derivatives.eq.server.http.response.model.PricingResponse;
import com.rebusland.fin.derivatives.eq.server.output.model.PricingResults;
import com.rebusland.fin.derivatives.eq.server.pricing.jni.PricingJNILayerHandler;

@RestController
@RequestMapping(path = "/")
public class PricingEndpoint {

	// TODO replace appropriately with atomic counterpart in a concurrent context
	private static long responseId = 1L;

	/**
	 * POST endpoint managing the invocation of the pricing library.
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE, path = UrlPaths.PRICING)
	public PricingResponse exampleJodaPost(@RequestBody PricingRequest reqBody) {
		final String results = PricingJNILayerHandler.EvaluateBlack(reqBody.getMktData().toJson(), reqBody.getMonteCarloSettings().toJson(), reqBody.getProduct().toJson());
		return PricingResponse.builder()
			.id(responseId)
			.pricingResults(PricingResults.fromJson(results, PricingResults.class))
			.build();
	}
}
