package com.rebusland.fin.derivatives.eq.server.http.endpoint;

public class UrlPaths {
	public static final String EXAMPLE_GET = "example/get";
	public static final String EXAMPLE_POST = "example/post";
	public static final String PRICING = "pricing";
}
