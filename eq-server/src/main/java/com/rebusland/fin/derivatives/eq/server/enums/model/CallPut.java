package com.rebusland.fin.derivatives.eq.server.enums.model;

public enum CallPut {
	CALL,
	PUT
}
