package com.rebusland.fin.derivatives.eq.server.http.converter;

import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import org.joda.beans.Bean;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Component;

import com.rebusland.fin.derivatives.eq.server.common.model.SerializableBeanFacade;

/**
 * This class enable the encoding/decoding of Joda Beans in Spring MVC (both REST and regular controllers).
 * If we're using Spring Boot we can avoid implementing the WebMvcConfigurer and adding all the Message Converters.
 * We can just define different HttpMessageConverter beans in the context, and Spring Boot will add them automatically to the autoconfiguration that it creates.
 * Otherwise, without Spring Boot, in order to use this message converter, it is mandatory declaring it in configuration i.e.
 * <pre class="code">
 * &#064;Configuration
 * public class TokenAuthWebConfig extends WebMvcConfigurerAdapter {
 *
 *     &#064;Override
 *    public void configureMessageConverters(List&lt;HttpMessageConverter&lt;?&gt;&gt; messageConverters) {
 *         messageConverters.add(new JodaBeanConverter());
 *     }
 * }</pre>
 * @see <a href="https://www.baeldung.com/spring-httpmessageconverter-rest">HTTP message converters with the Spring framework</a>
 */
@Component
public class JodaBeanHttpMsgConverter extends AbstractGenericHttpMessageConverter<Bean> {

	private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

	public JodaBeanHttpMsgConverter() {
		super(MediaType.APPLICATION_JSON, new MediaType("application", "*+json"));
		this.setDefaultCharset(DEFAULT_CHARSET);
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return Bean.class.isAssignableFrom(clazz);
	}

	@Override
	protected void writeInternal(Bean abstractBean, Type type, HttpOutputMessage outputMessage) throws HttpMessageNotWritableException {
		Objects.requireNonNull(abstractBean);
		try {
			String json = SerializableBeanFacade.toJson(abstractBean);
			outputMessage.getBody().write(json.getBytes(DEFAULT_CHARSET));
		} catch (IOException e) {
			throw new HttpMessageNotWritableException(this.getClass().getSimpleName() + " unable to convert bean [" + String.valueOf(abstractBean) + "]");
		}
	}

	@Override
	public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
		if (type instanceof Class<?>) {
			return canRead((Class<?>) type, mediaType);
		}
		return false;
	}

	@Override
	protected Bean readInternal(Class<? extends Bean> clazz, HttpInputMessage inputMessage) throws HttpMessageNotReadableException {

		try (InputStreamReader isr = new InputStreamReader(inputMessage.getBody(), DEFAULT_CHARSET)) {
			Bean bean = SerializableBeanFacade.fromJson(isr, clazz);

			return bean;
		} catch (IOException e) {
			throw new HttpMessageNotReadableException(this.getClass().getSimpleName() + "Unable to read message", e, inputMessage);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Bean read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws HttpMessageNotReadableException {
		if (type instanceof Class<?>) {
			final Class<?> unsafeClass = (Class<?>) type;
			try (InputStreamReader isr = new InputStreamReader(inputMessage.getBody(), DEFAULT_CHARSET)) {
				if (!Bean.class.isAssignableFrom(unsafeClass)) {
					throw new HttpMessageNotReadableException(this.getClass().getSimpleName() + " is unable to deserialize class [" + unsafeClass + "]", inputMessage);
				}
				return SerializableBeanFacade.fromJson(isr, (Class<Bean>) unsafeClass);
			} catch (HttpMessageNotReadableException e) {
				throw e;
			} catch (Exception e) {
				throw new HttpMessageNotReadableException(this.getClass().getSimpleName() + "Unable to read message", e, inputMessage);
			}
		}
		throw new HttpMessageNotReadableException("Unable to convert type [" + type.getTypeName() + "]", inputMessage);
	}
}
