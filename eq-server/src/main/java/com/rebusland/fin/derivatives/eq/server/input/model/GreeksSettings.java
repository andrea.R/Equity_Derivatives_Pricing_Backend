package com.rebusland.fin.derivatives.eq.server.input.model;

import org.joda.beans.ImmutableBean;
import org.joda.beans.gen.BeanDefinition;
import org.joda.beans.gen.PropertyDefinition;

import com.rebusland.fin.derivatives.eq.server.common.model.SerializableBeanFacade;
import java.util.Map;
import java.util.NoSuchElementException;
import org.joda.beans.Bean;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaBean;
import org.joda.beans.MetaProperty;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;

@BeanDefinition
public class GreeksSettings extends SerializableBeanFacade implements ImmutableBean {
	@PropertyDefinition (validate = "notNull")
	private final Double spotRelativeShift;

	//------------------------- AUTOGENERATED START -------------------------
	/**
	 * The meta-bean for {@code GreeksSettings}.
	 * @return the meta-bean, not null
	 */
	public static GreeksSettings.Meta meta() {
		return GreeksSettings.Meta.INSTANCE;
	}

	static {
		MetaBean.register(GreeksSettings.Meta.INSTANCE);
	}

	/**
	 * Returns a builder used to create an instance of the bean.
	 * @return the builder, not null
	 */
	public static GreeksSettings.Builder builder() {
		return new GreeksSettings.Builder();
	}

	/**
	 * Restricted constructor.
	 * @param builder  the builder to copy from, not null
	 */
	protected GreeksSettings(GreeksSettings.Builder builder) {
		JodaBeanUtils.notNull(builder.spotRelativeShift, "spotRelativeShift");
		this.spotRelativeShift = builder.spotRelativeShift;
	}

	@Override
	public GreeksSettings.Meta metaBean() {
		return GreeksSettings.Meta.INSTANCE;
	}

	//-----------------------------------------------------------------------
	/**
	 * Gets the spotRelativeShift.
	 * @return the value of the property, not null
	 */
	public Double getSpotRelativeShift() {
		return spotRelativeShift;
	}

	//-----------------------------------------------------------------------
	/**
	 * Returns a builder that allows this bean to be mutated.
	 * @return the mutable builder, not null
	 */
	public Builder toBuilder() {
		return new Builder(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj != null && obj.getClass() == this.getClass()) {
			GreeksSettings other = (GreeksSettings) obj;
			return JodaBeanUtils.equal(spotRelativeShift, other.spotRelativeShift);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = getClass().hashCode();
		hash = hash * 31 + JodaBeanUtils.hashCode(spotRelativeShift);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(64);
		buf.append("GreeksSettings{");
		int len = buf.length();
		toString(buf);
		if (buf.length() > len) {
			buf.setLength(buf.length() - 2);
		}
		buf.append('}');
		return buf.toString();
	}

	protected void toString(StringBuilder buf) {
		buf.append("spotRelativeShift").append('=').append(JodaBeanUtils.toString(spotRelativeShift)).append(',').append(' ');
	}

	//-----------------------------------------------------------------------
	/**
	 * The meta-bean for {@code GreeksSettings}.
	 */
	public static class Meta extends DirectMetaBean {
		/**
		 * The singleton instance of the meta-bean.
		 */
		static final Meta INSTANCE = new Meta();

		/**
		 * The meta-property for the {@code spotRelativeShift} property.
		 */
		private final MetaProperty<Double> spotRelativeShift = DirectMetaProperty.ofImmutable(
				this, "spotRelativeShift", GreeksSettings.class, Double.class);
		/**
		 * The meta-properties.
		 */
		private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(
				this, null,
				"spotRelativeShift");

		/**
		 * Restricted constructor.
		 */
		protected Meta() {
		}

		@Override
		protected MetaProperty<?> metaPropertyGet(String propertyName) {
			switch (propertyName.hashCode()) {
				case -407319756:  // spotRelativeShift
					return spotRelativeShift;
			}
			return super.metaPropertyGet(propertyName);
		}

		@Override
		public GreeksSettings.Builder builder() {
			return new GreeksSettings.Builder();
		}

		@Override
		public Class<? extends GreeksSettings> beanType() {
			return GreeksSettings.class;
		}

		@Override
		public Map<String, MetaProperty<?>> metaPropertyMap() {
			return metaPropertyMap$;
		}

		//-----------------------------------------------------------------------
		/**
		 * The meta-property for the {@code spotRelativeShift} property.
		 * @return the meta-property, not null
		 */
		public final MetaProperty<Double> spotRelativeShift() {
			return spotRelativeShift;
		}

		//-----------------------------------------------------------------------
		@Override
		protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
			switch (propertyName.hashCode()) {
				case -407319756:  // spotRelativeShift
					return ((GreeksSettings) bean).getSpotRelativeShift();
			}
			return super.propertyGet(bean, propertyName, quiet);
		}

		@Override
		protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
			metaProperty(propertyName);
			if (quiet) {
				return;
			}
			throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
		}

	}

	//-----------------------------------------------------------------------
	/**
	 * The bean-builder for {@code GreeksSettings}.
	 */
	public static class Builder extends DirectFieldsBeanBuilder<GreeksSettings> {

		private Double spotRelativeShift;

		/**
		 * Restricted constructor.
		 */
		protected Builder() {
		}

		/**
		 * Restricted copy constructor.
		 * @param beanToCopy  the bean to copy from, not null
		 */
		protected Builder(GreeksSettings beanToCopy) {
			this.spotRelativeShift = beanToCopy.getSpotRelativeShift();
		}

		//-----------------------------------------------------------------------
		@Override
		public Object get(String propertyName) {
			switch (propertyName.hashCode()) {
				case -407319756:  // spotRelativeShift
					return spotRelativeShift;
				default:
					throw new NoSuchElementException("Unknown property: " + propertyName);
			}
		}

		@Override
		public Builder set(String propertyName, Object newValue) {
			switch (propertyName.hashCode()) {
				case -407319756:  // spotRelativeShift
					this.spotRelativeShift = (Double) newValue;
					break;
				default:
					throw new NoSuchElementException("Unknown property: " + propertyName);
			}
			return this;
		}

		@Override
		public Builder set(MetaProperty<?> property, Object value) {
			super.set(property, value);
			return this;
		}

		@Override
		public GreeksSettings build() {
			return new GreeksSettings(this);
		}

		//-----------------------------------------------------------------------
		/**
		 * Sets the spotRelativeShift.
		 * @param spotRelativeShift  the new value, not null
		 * @return this, for chaining, not null
		 */
		public Builder spotRelativeShift(Double spotRelativeShift) {
			JodaBeanUtils.notNull(spotRelativeShift, "spotRelativeShift");
			this.spotRelativeShift = spotRelativeShift;
			return this;
		}

		//-----------------------------------------------------------------------
		@Override
		public String toString() {
			StringBuilder buf = new StringBuilder(64);
			buf.append("GreeksSettings.Builder{");
			int len = buf.length();
			toString(buf);
			if (buf.length() > len) {
				buf.setLength(buf.length() - 2);
			}
			buf.append('}');
			return buf.toString();
		}

		protected void toString(StringBuilder buf) {
			buf.append("spotRelativeShift").append('=').append(JodaBeanUtils.toString(spotRelativeShift)).append(',').append(' ');
		}

	}

	//-------------------------- AUTOGENERATED END --------------------------
}
