package com.rebusland.fin.derivatives.eq.server.http.config;

import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.rebusland.fin.derivatives.eq.server.http.converter.JodaBeanHttpMsgConverter;

/**
 * To customize the default configuration in Java you simply implement the WebMvcConfigurer interface
 * or more likely extend the class WebMvcConfigurerAdapter and override the methods you need.
 * 
 * NOTA BENE: If we're using Spring Boot we can avoid implementing the WebMvcConfigurer and adding all the Message Converters.
 * We can just define different HttpMessageConverter beans in the context, and Spring Boot will add them
 * automatically to the autoconfiguration that it creates!!
 * @see <a href="https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mvc.html#mvc-config">Web MVC Framework</a>
 * @see <a href="https://www.baeldung.com/spring-httpmessageconverter-rest">HTTP message converters with the Spring framework</a>
 */
//@Configuration
//@EnableWebMvc
//public class WebConfig implements WebMvcConfigurer {
//	private static final long TIMEOUT = 5; // seconds
//
//	@Override
//	public void configureAsyncSupport(AsyncSupportConfigurer asyncSupportConfigurer) {
//		// TODO move timeout duration to config
//		final Duration defaultAsyncTimeout = Duration.ofSeconds(TIMEOUT);
//		asyncSupportConfigurer.setDefaultTimeout(defaultAsyncTimeout.toMillis()); // in millis
//	}
//
//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
//		messageConverters.add(new JodaBeanHttpMsgConverter());
//		messageConverters.add(new StringHttpMessageConverter(StandardCharsets.UTF_8));
//		messageConverters.add(new ByteArrayHttpMessageConverter());
//		messageConverters.add(new MappingJackson2HttpMessageConverter());
//
//		// TODO log instead
//		System.out.println("Set a custom configuration for the HttpMessageConverters");
//	}
//}
