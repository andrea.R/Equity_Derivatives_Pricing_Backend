package com.rebusland.fin.derivatives.eq.server.enums.model;

public enum SimulationScheduler {
	SEQUENTIAL,
	MULTITHREAD
}
