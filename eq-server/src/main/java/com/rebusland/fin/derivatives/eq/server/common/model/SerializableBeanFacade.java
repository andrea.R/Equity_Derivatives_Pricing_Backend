package com.rebusland.fin.derivatives.eq.server.common.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.joda.beans.Bean;
import org.joda.beans.ser.JodaBeanSer;
import org.joda.beans.ser.json.JodaBeanJsonReader;
import org.joda.beans.ser.json.JodaBeanJsonWriter;
import org.joda.beans.ser.xml.JodaBeanXmlReader;
import org.joda.beans.ser.xml.JodaBeanXmlWriter;

/**
 * A facade simplifying Joda beans serialization and deserialization (mainly from/to JSON).
 */
public abstract class SerializableBeanFacade implements Bean {

	public static <T> T fromJson(String s, Class<T> c) {
		return newJsonReader().read(s, c);
	}

	public static <T> T fromJsonReader(Reader reader, Class<T> c) {
		return newJsonReader().read(reader, c);
	}

	public static <T> T fromJsonFile(File file, Class<T> c) throws IOException {
		try (InputStream is = new FileInputStream(file); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newJsonReader().read(reader, c);
		}
	}

	public static <T> T fromJsonFile(String filename, Class<T> c) throws IOException {
		try (InputStream is = new FileInputStream(filename); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newJsonReader().read(reader, c);
		}
	}

	public static <T> T fromJsonResource(String name, Class<T> c) throws IOException {
		return fromJsonResource(name, getCurrentClassLoader(), c);
	}

	public static <T> T fromJsonResource(String name, ClassLoader loader, Class<T> c) throws IOException {
		try (InputStream is = getResourceAsStream(name, loader); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newJsonReader().read(reader, c);
		}
	}

	public static <T> T fromXml(String s, Class<T> c) {
		return newXmlReader().read(s, c);
	}

	public static <T> T fromXmlReader(Reader reader, Class<T> c) {
		return newXmlReader().read(reader, c);
	}

	public static <T> T fromXmlFile(File file, Class<T> c) throws IOException {
		try (InputStream is = new FileInputStream(file); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newXmlReader().read(reader, c);
		}
	}

	public static <T> T fromXmlFile(String filename, Class<T> c) throws IOException {
		try (InputStream is = new FileInputStream(filename); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newXmlReader().read(reader, c);
		}
	}

	public static <T> T fromXmlResource(String name, Class<T> c) throws IOException {
		return fromXmlResource(name, getCurrentClassLoader(), c);
	}

	public static <T> T fromXmlResource(String name, ClassLoader loader, Class<T> c) throws IOException {
		try (InputStream is = getResourceAsStream(name, loader); Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {
			return newXmlReader().read(reader, c);
		}
	}

	public static <T extends Bean> String toJson(T bean) {
		Objects.requireNonNull(bean);
		return JodaBeanSer.COMPACT.jsonWriter().write(bean, false);
	}

	public static <T extends Bean> Bean fromJson(Reader reader, Class<T> clazz) {
		return JodaBeanSer.COMPACT.jsonReader().read(reader, clazz);
	}

	public String toJson() {
		return toJson(true);
	}

	public String toJson(boolean compact) {
		return newJsonWriter(compact).write(this, false);
	}

	public void toJsonWriter(Writer writer) throws IOException {
		toJsonWriter(writer, true);
	}

	public void toJsonWriter(Writer writer, boolean compact) throws IOException {
		newJsonWriter(compact).write(this, false, writer);
	}

	public void toJsonFile(File file) throws IOException {
		toJsonFile(file, true);
	}

	public void toJsonFile(File file, boolean compact) throws IOException {
		try (OutputStream os = new FileOutputStream(file, false); Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8)) {
			newJsonWriter(compact).write(this, false, writer);
		}
	}

	public void toJsonFile(String filename) throws IOException {
		toJsonFile(filename, true);
	}

	public void toJsonFile(String filename, boolean compact) throws IOException {
		try (OutputStream os = new FileOutputStream(filename, false); Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8)) {
			newJsonWriter(compact).write(this, false, writer);
		}
	}

	public String toXml() {
		return toXml(true);
	}

	public String toXml(boolean compact) {
		return newXmlWriter(compact).write(this, false);
	}

	public void toXmlWriter(Writer writer) throws IOException {
		toXmlWriter(writer, true);
	}

	public void toXmlWriter(Writer writer, boolean compact) throws IOException {
		writer.append(this.toXml(compact));
	}

	public void toXmlFile(File file) throws IOException {
		toXmlFile(file, true);
	}

	public void toXmlFile(File file, boolean compact) throws IOException {
		try (OutputStream os = new FileOutputStream(file, false); Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8)) {
			writer.append(this.toXml(compact));
		}
	}

	public void toXmlFile(String filename) throws IOException {
		toXmlFile(filename, true);
	}

	public void toXmlFile(String filename, boolean compact) throws IOException {
		try (OutputStream os = new FileOutputStream(filename, false); Writer writer = new OutputStreamWriter(os, StandardCharsets.UTF_8)) {
			writer.append(this.toXml(compact));
		}
	}

	/*
	 * A new instance of the reader must be created for each message.
	 */
	private static JodaBeanJsonReader newJsonReader() {
		return JodaBeanSer.COMPACT.jsonReader();
	}

	/*
	 * A new instance of the writer must be created for each message.
	 */
	private static JodaBeanJsonWriter newJsonWriter(boolean compact) {
		if (compact) {
			return JodaBeanSer.COMPACT.jsonWriter();
		}
		return JodaBeanSer.PRETTY.withIndent("\t").jsonWriter();
	}

	/*
	 * A new instance of the reader must be created for each message.
	 */
	private static JodaBeanXmlReader newXmlReader() {
		return JodaBeanSer.COMPACT.xmlReader();
	}

	/*
	 * A new instance of the writer must be created for each message.
	 */
	private static JodaBeanXmlWriter newXmlWriter(boolean compact) {
		if (compact) {
			return JodaBeanSer.COMPACT.xmlWriter();
		}
		return JodaBeanSer.PRETTY.xmlWriter();
	}

	private static ClassLoader getCurrentClassLoader() {
		return Thread.currentThread().getContextClassLoader();
	}

	private static InputStream getResourceAsStream(String name, ClassLoader loader) throws IOException {
		InputStream is = loader.getResourceAsStream(name);
		if (is == null) {
			throw new IOException(name + " (The resource could not be found)");
		}
		return is;
	}
}
