package com.rebusland.fin.derivatives.eq.server.enums.model;

public enum VarianceReduction {
	ANTITHETIC,
	CONTROL
}
