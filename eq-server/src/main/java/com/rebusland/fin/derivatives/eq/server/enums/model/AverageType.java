package com.rebusland.fin.derivatives.eq.server.enums.model;

public enum AverageType {
	ARITHMETIC,
	GEOMETRIC
}
