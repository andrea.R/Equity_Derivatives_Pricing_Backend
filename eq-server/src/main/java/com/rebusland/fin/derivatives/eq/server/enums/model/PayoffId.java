package com.rebusland.fin.derivatives.eq.server.enums.model;

/**
 * The label marking each financial product our pricing engine supports.
 */
public enum PayoffId {
	ASIAN,
	EUROPEAN_STD
}
