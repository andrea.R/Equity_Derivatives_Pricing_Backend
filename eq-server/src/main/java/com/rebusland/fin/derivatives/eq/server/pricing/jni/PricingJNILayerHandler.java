package com.rebusland.fin.derivatives.eq.server.pricing.jni;

public class PricingJNILayerHandler {
	// TODO check: these shouldn't be necessary once the native dll is added to the Java Library Path
	private static final String NATIVE_LIB_PATH = "src/main/resources/native/Release/";
	// Extension omitted: assumed .so when run in Unix environment, and .dll when run in Windows one
	// From System.loadLibrary(..) doc: <<The libname argument must not contain any platformspecific prefix, file extension or path>>
	private static final String NATIVE_LIB_NAME = "libEQPricer";

	private static final String JNI_LAYER_LIB_PATH = "src/main/resources/jni/lib/";
	// Extension omitted: assumed .so when run in Unix environment, and .dll when run in Windows one
	private static final String JNI_LAYER_LIB_NAME = "libjni_layer";

	// NB We have to explicitly load also the actual native library the JNI layer uses.
	// TODO check: I wasn't able to specify the java.library.path or perhaps this is not used
	// when searching for the dependent shared libraries.
	static {
		System.loadLibrary(NATIVE_LIB_PATH + NATIVE_LIB_NAME);
		System.loadLibrary(JNI_LAYER_LIB_PATH + JNI_LAYER_LIB_NAME);
    }

	// Declare native methods wrapping the underlying native library pricing endpoints
	public static native String EvaluateBlack(String flatMktDataJSONString, String mcSettingsJSONString, String productJSONString);
}
